# Translation for kirigami-addons.po to Euskara/Basque (eu).
# Copyright (C) 2021-2023 This file is copyright:
# This file is distributed under the same license as the kirigami-addons package.
# KDE euskaratzeko proiektuko arduraduna <xalba@ni.eus>.
#
# Translators:
# Iñigo Salvador Azurmendi <xalba@ni.eus>, 2021, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: kirigami-addons\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-02-26 00:57+0000\n"
"PO-Revision-Date: 2023-01-23 08:14+0100\n"
"Last-Translator: Iñigo Salvador Azurmendi <xalba@ni.eus>\n"
"Language-Team: Basque <kde-i18n-eu@kde.org>\n"
"Language: eu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.12.1\n"

#: dateandtime/DatePicker.qml:141
#, kde-format
msgctxt "%1 is month name, %2 is year"
msgid "%1 %2"
msgstr "%2(e)ko %1"

#: dateandtime/DatePicker.qml:182
#, kde-format
msgctxt "kirigami-addons"
msgid "Days"
msgstr "Egunak"

#: dateandtime/DatePicker.qml:190
#, kde-format
msgctxt "kirigami-addons"
msgid "Months"
msgstr "Hilabeteak"

#: dateandtime/DatePicker.qml:196
#, kde-format
msgctxt "kirigami-addons"
msgid "Years"
msgstr "Urteak"

#: dateandtime/DatePopup.qml:55
#, kde-format
msgid "Previous"
msgstr "Aurrekoa"

#: dateandtime/DatePopup.qml:64
#, kde-format
msgid "Today"
msgstr "Gaur"

#: dateandtime/DatePopup.qml:73
#, kde-format
msgid "Next"
msgstr "Hurrengoa"

#: dateandtime/DatePopup.qml:92
#, kde-format
msgid "Cancel"
msgstr "Utzi"

#: dateandtime/DatePopup.qml:101
#, kde-format
msgid "Accept"
msgstr "Onartu"

#: dateandtime/private/TumblerTimePicker.qml:88
#, kde-format
msgctxt "Time separator"
msgid ":"
msgstr ":"

#: mobileform/AboutKDE.qml:23
#, kde-format
msgid "About KDE"
msgstr "KDEri buruz"

#: mobileform/AboutKDE.qml:55
#, kde-format
msgid "KDE"
msgstr "KDE"

#: mobileform/AboutKDE.qml:64
#, kde-format
msgid "Be Free!"
msgstr "Izan aske!"

#: mobileform/AboutKDE.qml:73
#, kde-format
msgid ""
"KDE is a world-wide community of software engineers, artists, writers, "
"translators and creators who are committed to Free Software development. KDE "
"produces the Plasma desktop environment, hundreds of applications, and the "
"many software libraries that support them.\n"
msgstr ""
"KDE Software Askearen garapenarekin engaiatutako mundu osoko software "
"ingeniari, artista, idazle, itzultzaile eta sortzaile komunitate bat da. "
"KDEk Plasma mahaigain ingurunea, ehunka aplikazio, eta haiei sostengua "
"ematen dieten liburutegi ugariak ekoizten ditu.\n"

#: mobileform/AboutKDE.qml:81 mobileform/AboutPage.qml:182
#, kde-format
msgid "Homepage"
msgstr "Orri nagusia"

#: mobileform/AboutKDE.qml:94
#, kde-format
msgid ""
"Software can always be improved, and the KDE team is ready to do so. "
"However, you - the user - must tell us when something does not work as "
"expected or could be done better.\n"
msgstr ""
"Softwarea beti hobetu daiteke, eta KDE taldea hobekuntza horiek egiteko "
"prest dago. Hala ere, zuk -erabiltzaileak-, esan behar diguzu zerbait behar "
"bezala noiz ez dabilen edo hobetu daitekeen.\n"

#: mobileform/AboutKDE.qml:103 mobileform/AboutPage.qml:221
#, kde-format
msgid "Report a bug"
msgstr "Eman akats baten berri"

#: mobileform/AboutKDE.qml:116
#, kde-format
msgid ""
"You do not have to be a software developer to be a member of the KDE team. "
"You can join the national teams that translate program interfaces. You can "
"provide graphics, themes, sounds, and improved documentation. You decide!"
msgstr ""
"Ez duzu software programatzailea izan behar KDE taldeko kide izateko. "
"Programen interfazeak itzultzen dituzten hizkuntza-taldeetan parte har "
"dezakezu. Irudiak, gaiak, soinuak eta dokumentu hobetuekin lagun dezakezu!"

#: mobileform/AboutKDE.qml:123 mobileform/AboutPage.qml:200
#, kde-format
msgid "Get Involved"
msgstr "Engaia zaitez"

#: mobileform/AboutKDE.qml:130
#, kde-format
msgid "Developer Documentation"
msgstr "Garatzailearen dokumentazioa"

#: mobileform/AboutKDE.qml:143
#, kde-format
msgid ""
"KDE software is and will always be available free of charge, however "
"creating it is not free.\n"
msgstr ""
"KDE softwarea doan eskuragarri dago eta beti egongo da, hala ere, hura "
"sortzea ez da doan.\n"

#: mobileform/AboutKDE.qml:154
#, kde-format
msgid "KDE e.V"
msgstr "KDE e.V"

#: mobileform/AboutKDE.qml:161 mobileform/AboutPage.qml:191
#, kde-format
msgid "Donate"
msgstr "Egin dohaintza"

#: mobileform/AboutPage.qml:87
#, kde-format
msgid "About %1"
msgstr "%1(e)ri buruz"

#: mobileform/AboutPage.qml:138
#, kde-format
msgid "Copyright"
msgstr "Copyright"

#: mobileform/AboutPage.qml:145
#, kde-format
msgid "License"
msgid_plural "Licenses"
msgstr[0] "Lizentzia"
msgstr[1] "Lizentziak"

#: mobileform/AboutPage.qml:234
#, kde-format
msgid "Libraries in use"
msgstr "Erabiltzen ari diren liburutegiak"

#: mobileform/AboutPage.qml:265
#, kde-format
msgid "Authors"
msgstr "Egileak"

#: mobileform/AboutPage.qml:277
#, kde-format
msgid "Credits"
msgstr "Merituak"

#: mobileform/AboutPage.qml:289
#, kde-format
msgid "Translators"
msgstr "Itzultzaileak"
